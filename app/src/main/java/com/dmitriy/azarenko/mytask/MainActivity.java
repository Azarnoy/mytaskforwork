package com.dmitriy.azarenko.mytask;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    final String LOG_TAG = "myLogs";

    ListView listView;

    TextView name;
    TextView subcategory_name;
    TextView geometry_name;
    TextView rating;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (TextView) findViewById(R.id.name_Id);
        subcategory_name = (TextView) findViewById(R.id.subcategory_Id);
        geometry_name = (TextView) findViewById(R.id.geometry_Id);
        rating = (TextView) findViewById(R.id.rating_Id);
        listView = (ListView) findViewById(R.id.listVieww);

//                load data from the internal storage
        FileInputStream fis;
        try {
            fis = openFileInput("countries_file");
            ObjectInputStream ois = new ObjectInputStream(fis);
            final ArrayList<Places> returnlist = (ArrayList<Places>) ois.readObject();
            ois.close();
            Log.d(LOG_TAG, "загрузка данных из внютренней памяти");
            listView.setAdapter(new MyAdapter(this, returnlist));
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent mIntent = new Intent(MainActivity.this, MapActivity.class);
                    Places placez = returnlist.get(position);
                    mIntent.putExtra("key", placez);
                    startActivity(mIntent);

                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

//               get the data from Retrofit
        Retrofit.getPlaces(new Callback<List<Places>>() {
            @Override
            public void success(final List<Places> places, Response response) {
                Log.d(LOG_TAG, "получили данные");

//                save data into internal storage
                FileOutputStream fos = null;
                try {
                    fos = openFileOutput("countries_file", Context.MODE_PRIVATE);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(places);
                    oos.close();
                    Log.d(LOG_TAG, "сохранение данных во внутреннюю память");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

//                initialize the adapter
                listView.setAdapter(new MyAdapter(MainActivity.this, places));
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent mIntent = new Intent(MainActivity.this, MapActivity.class);
                        Places plasez = places.get(position);
                        mIntent.putExtra("key", plasez);
                        startActivity(mIntent);
                    }
                });


            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_SHORT).show();
                Log.d(LOG_TAG, "данные не получены");


            }
        });


    }


    //                     create the adapter for listview
    class MyAdapter extends ArrayAdapter<Places> {

        private Context context;


        public MyAdapter(Context context, List<Places> objects) {
            super(context, R.layout.list_item, objects);
            this.context = context;


        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            View rowView = convertView;
            if (rowView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(R.layout.list_item, parent, false);
                holder = new ViewHolder();
                holder.nameOfPlace = (TextView) rowView.findViewById(R.id.name_Id);
                holder.subcategory_name = (TextView) rowView.findViewById(R.id.subcategory_Id);
                holder.geometryName = (TextView) rowView.findViewById(R.id.geometry_Id);
                holder.imageView = (ImageView) rowView.findViewById(R.id.imageView);
                holder.imageView2 = (ImageView) rowView.findViewById(R.id.imageView2);
                holder.rating = (TextView) rowView.findViewById(R.id.rating_Id);
                rowView.setTag(holder);
            } else {
                holder = (ViewHolder) rowView.getTag();
            }
//            transfer data into adapter
            Places places = getItem(position);
            holder.nameOfPlace.setText(places.getName());
            holder.subcategory_name.setText(places.getSubcategory_name());
            holder.geometryName.setText(places.getGeometry_name());
            holder.imageView.setImageResource(R.drawable.changing);
            holder.imageView2.setImageResource(R.drawable.love_5033);
            holder.rating.setText("Рейтинг: " + places.getRating() + "           В избранном: " + places.getFavorite());


//             get images into adapter
            if (!TextUtils.isEmpty(places.getCsv_image())) {
                Picasso.with(context)
                        .load(places.getCsv_image())
                        .placeholder(R.drawable.changing)
                        .transform(new RoundedTransformation(50, 4))
                        .resize(150, 150)
                        .into(holder.imageView);
            }

            Log.d(LOG_TAG, "Получение данных адаптером");

            return rowView;
        }

        class ViewHolder {

            public TextView nameOfPlace;
            public TextView subcategory_name;
            public TextView geometryName;
            public TextView rating;
            public ImageView imageView;
            public ImageView imageView2;

        }
    }

    //         rounded transformation for image
    public class RoundedTransformation implements com.squareup.picasso.Transformation {
        private final int radius;
        private final int margin;  // dp

        // radius is corner radii in dp
        // margin is the board in dp
        public RoundedTransformation(final int radius, final int margin) {
            this.radius = radius;
            this.margin = margin;
        }

        @Override
        public Bitmap transform(final Bitmap source) {


            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(new BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

            Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            canvas.drawRoundRect(new RectF(margin, margin, source.getWidth() - margin, source.getHeight() - margin), radius, radius, paint);

            if (source != output) {
                source.recycle();
            }


            return output;
        }

        @Override
        public String key() {
            return "rounded";
        }
    }



}
