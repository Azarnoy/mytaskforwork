package com.dmitriy.azarenko.mytask;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

public class Retrofit {

    private static final String ENDPOINT = "http://gdetut.com/api";
    private static ApiInterface apiInterface;

    interface ApiInterface {
        @GET("/firms?salt=63926e380bdc96ef990d57898daeb71c&category_id=1")
        void getPlaces(Callback<List<Places>> callback);


    }

    static {
        init();
    }

    private static void init() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getPlaces(Callback<List<Places>> callback) {
        apiInterface.getPlaces(callback);
    }


}