package com.dmitriy.azarenko.mytask;

import java.io.Serializable;


public class Places implements Serializable {


    String name;
    String geometry_name;
    String rating;
    String subcategory_name;
    String favorite;
    String csv_image;
    String lon;
    String lat;

    public Places(String name, String geometry_name, String rating, String subcategory_name, String favorite, String csv_image, String lon, String lat) {
        this.name = name;
        this.geometry_name = geometry_name;
        this.rating = rating;
        this.subcategory_name = subcategory_name;
        this.favorite = favorite;
        this.csv_image = csv_image;
        this.lon = lon;
        this.lat = lat;

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getGeometry_name() {
        return geometry_name;
    }

    public void setGeometry_name(String geometry_name) {
        this.geometry_name = geometry_name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getSubcategory_name() {
        return subcategory_name;
    }

    public void setSubcategory_name(String subcategory_name) {
        this.subcategory_name = subcategory_name;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    public String getCsv_image() {
        return csv_image;
    }

    public void setCsv_image(String csv_image) {
        this.csv_image = csv_image;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }
}
