package com.dmitriy.azarenko.mytask;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String LOG_TAG = "myLogs";
    Marker marker;
    String nameOfPlace;
    String categoryOfPlace;
    String ratingOfPlace;
    double lon;
    double lat;
    GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);


        Places places = (Places) getIntent().getExtras().getSerializable("key");
        nameOfPlace = places.getName();
        categoryOfPlace = places.getSubcategory_name();
        ratingOfPlace = places.getRating();
        lon = Double.parseDouble(places.getLon());
        lat = Double.parseDouble(places.getLat());

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);


    }


    //               load the map and set marker
    @Override
    public void onMapReady(final GoogleMap map) {


        marker = map.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lon))
                .title(nameOfPlace)
                .snippet(categoryOfPlace + "Рейтинг:" + ratingOfPlace));


        marker.showInfoWindow();

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {

                return null;
            }

            //                         set view for marker info
            @Override
            public View getInfoContents(Marker marker) {
                View v = getLayoutInflater().inflate(R.layout.marker_info, null);


                TextView mN = (TextView) v.findViewById(R.id.marker_name_id);
                TextView mC = (TextView) v.findViewById(R.id.marker_category_id);
                TextView mR = (TextView) v.findViewById(R.id.marker_rating_id);
                if (mN != null) {
                    mN.setText(nameOfPlace);
                }
                if (mC != null) {
                    mC.setText(categoryOfPlace);
                }
                if (mR != null) {
                    mR.setText("Рейтинг: " + ratingOfPlace);
                }


                return v;
            }
        });


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
//        zooming camera
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 10));
        map.animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);
        map.setMyLocationEnabled(true);
        Log.d(LOG_TAG, "map is load");


    }


}
